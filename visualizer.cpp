// ======================================================================
// visualizer widget
// Copyright (C) 2015 by David Francisco Ayerbe
//
// visualizer.cpp - OpenGL widget class
//
// Written by: David Francisco Ayerbe
// ======================================================================

#include "visualizer.h"
#include "MainWindow.h"
#include "GL/glu.h"
#include <QOpenGLTexture>
#include <map>


using namespace std;

VisualizerWidget::VisualizerWidget(QWidget *parent) :
QOpenGLWidget(parent)
{
	timer = new QTimer(this);
	color = 0;
	
	
}

void VisualizerWidget::initializeGL()
{
	// black background
	glClearColor(0, 0, 0, 0);

}

void VisualizerWidget::location()
{

	for (int i = 0; i < 10; i++){
		rando[i] = rand() % 10;
		rand_color[i] = rand() % 5;
		rando[i] = rando[i] / 10;
		
	}
	
	//color = (color + 1) % 6;

}

void VisualizerWidget::paintGL()
{
	glClear(GL_COLOR_BUFFER_BIT);

	double ver_L = -1;
	double ver_R = -0.8;
	for (int i = 0; i < 10; i++){



		if (rand_color[i] == 0)      glColor3f(0, 0, 1);   // blue
		else if (rand_color[i] == 1) glColor3f(1, 0, 0);   // red
		else if (rand_color[i] == 5) glColor3f(0, 1, 0);   // green
		else if (rand_color[i] == 3) glColor3f(1, 1, 0);   // yellow
		else if (rand_color[i] == 4) glColor3f(0, 1, 1);   // birght blue
		else if (rand_color[i] == 2) glColor3f(0.5, 1.0, 0.5); // light green
	
		glBegin(GL_QUADS);              
		{
			glVertex2f(ver_L, -1);     // Define vertices in counter-clockwise (CCW) order
			glVertex2f(ver_R, -1);     //  so that the normal (front-face) is facing you
			glVertex2f(ver_R, rando[i]);
			glVertex2f(ver_L, rando[i]);
		}
		ver_L += .2;
		ver_R += .2;
		glEnd();
	}




	location();
}
void VisualizerWidget::resizeGL(int width, int height)
{

	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}


void  VisualizerWidget::startTime()
{

	if (timer->isActive())
		return;
	timer->start(100);
	connect(timer, SIGNAL(timeout()), this, SLOT(repaint()));

}

void  VisualizerWidget::endTime()
{

	if (timer->isActive())
		timer->stop();
	glClearColor(0, 0, 0, 0);
	glClear(GL_COLOR_BUFFER_BIT);
}
void  VisualizerWidget::repaint()
{
	paintGL();
	update();
}
