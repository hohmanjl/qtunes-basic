#include <QApplication>
#include "MainWindow.h"


int main(int argc, char **argv)
{
	// init variables and application font
	QString program = argv[0];
	QApplication app(argc, argv);

	// invoke MainWindow constructor
	MainWindow window(program);

	window.setWindowIcon(QApplication::style()->standardIcon(QStyle::SP_DriveCDIcon));
	// window.setFixedHeight(156);
	window.setMinimumWidth(320);
	window.resize(800, 700);

	// display MainWindow
	window.show();

    return app.exec();
}
