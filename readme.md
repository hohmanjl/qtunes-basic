# qTunes-basic
## CSC 221 Spring 2015, The City College of New York
----------------------------------------------

Authors:

  * James Hohman
  * David Ayerbe
  * Professor George Wolberg (Instructional/Prototype Code)
  
Requires:

  * Qt v5.4.0
  * OpenGL
  * TagLib

## Description
----------------------------------------------
Basic audio player, based off of Prof. G. Wolberg's qTunes code samples.

An *Tunes clone for educational purposes.

## Compilation Help
----------------------------------------------
  * [ Tutorial for setting up TagLib and ZLib in Microsoft Visual Studio 2013 ](https://hohmanjl.wordpress.com/2015/03/06/taglib-and-zlib-for-microsoft-visual-studio-2013/)