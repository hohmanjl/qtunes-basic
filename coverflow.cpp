// ======================================================================
// coverflow widget
// Copyright (C) 2015 by James Hohman
//
// coverflow.cpp - OpenGL widget class
//
// Written by: James Hohman, 2015
// Based on Prof. Wolberg's Example.
// ======================================================================
#include "coverflow.h"
#include "MainWindow.h"
#include "GL/glu.h"
#include <QOpenGLTexture>
#include <map>


using namespace std;

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// CoverflowWidget QOpenGLWidget subclass, Constructor
// Customizations include dynamic spacing based on resize AR.
// Only draw up to m_draw_record_max records at a time for
// performance.
// Also asymptotic, dynamic animation duration based upon 
// distance traversed between records.
// James Hohman
CoverflowWidget::CoverflowWidget(QWidget *parent)
: QOpenGLWidget(parent)
{
	m_zoom = 0.0f; // zoom increment
	m_shift = 0;
	m_scrollOffset = 0.0f;  // -1<offset<1 current scroll position
	
	// We initialize scrollTime but we Reset it in scrollTimer!!! Upon
	// completion of the animation. This way we can have
	// dynamic scroll events.
	m_scrollTime = 15.0f;  // total scrolling time (per push)
	m_scrollUpdateInterval = 1;  // scrolling time increment
	m_bScrolling = 0;  // scrolling boolean: 1=true; 0=false
	m_redraw_rate = 17;  // 17 ms = 60 fps
	m_record_target = 0;  // Initialize in case user presses arrows.
	m_recordCount = 0; // Initialize in case user presses arrows.
	m_records_loaded = false; // set when record data has been loaded.
	m_spacing = 1.75f;  // spacing scaler between records.
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// G. Wolberg, James Hohman
void CoverflowWidget::initializeGL() {
	glEnable(GL_DEPTH_TEST);
	glClearColor(0, 0, 0, 0);
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// G. Wolberg, James Hohman
void CoverflowWidget::resizeGL(int width, int height) {
	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(70. + m_zoom, (float)width / height, 0.01, 1000);
	// Dynamically adjust spacing between records,
	// so that the likelyhood of seeing records appear and 
	// disappear from thin air is reduced. (Offscreen 
	// record entry/exit)
	m_spacing = ((float)width / height) / 2;
	if (m_spacing < 0.5f)
		m_spacing = 0.5f;
	else if (m_spacing > 2.5f)
		m_spacing = 2.5f;
	glMatrixMode(GL_MODELVIEW);
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// G. Wolberg, James Hohman
void CoverflowWidget::paintGL() {
	// clear color and depth buffers
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();
	glColor3f(1.0f, 1.0f, 1.0f);

	// draw all records
	drawRecords();
	glFlush();
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// init:
//
// Initialize data structure of records when given an album multimap.
// The input is recordCount, which is the number of record albums.
// This number should always be odd for display symmetry.
// James Hohman, G. Wolberg
//
void CoverflowWidget::init(multimap<QString, QString> *mmAlbum)
{
	// init global vars and create space
	m_recordCount = mmAlbum->size();
	//m_records = (Record*)malloc(sizeof(Record)* m_recordCount);
    m_records = new Record[m_recordCount];
	// init data structure with dimensions of cover art polygons
	// and filenames for the polygon textures
	int i = 0;
	for (multimap<QString, QString>::iterator it = mmAlbum->begin(); it != mmAlbum->end(); ++it)
	{
		// save dimensions
		m_records[i].width = 5;
		m_records[i].height = 5;

		// save image filenames
		strcpy(m_records[i].imageFilename, (*it).second.toLocal8Bit().data());

		// From MainWindow public method, get the id3 album art.
		m_records[i].qimg = ((MainWindow*)(parent()))->get_id3_album_art(m_records[i].imageFilename);
		i++;
	}
	m_record_target = m_recordCount / 2;
	// In the case of re-inits reset m_shift.
	m_shift = 0;
	m_records_loaded = true;
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// zoomRecords:
//
// Zoom in or out up (dir=-0.1) or down (dir=0.1).
// James Hohman
//
void CoverflowWidget::zoomRecords(float dir)
{
	// Set zoom.
	m_zoom += dir;
	update();
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// animateToIndex:
//
// Animate until we arrive at desired index. Uses asymptotic function to 
// dynamically adjust duration between animation such that farther 
// distances take longer, but does not increase without bound.
// Maximum animation duration is 300 frames for infinite scroll.
// James Hohman
//
void CoverflowWidget::animateToIndex(int target) {
	// Some sanity checks. Make sure target is in range.
	if ((target > m_recordCount - 1) || (target < 0) || (target == m_record_current) || (m_bScrolling))
		return;

	// Dynamically adjust scroll time based upon
	// number of records skipped.
	int quantity = abs(target - m_record_current);
	// Decided against log because it is 
	// an unbounded monotonically increasing function.
	// m_scrollTime = (log10(quantity) * 2 + 1) * 15.0f;
	// Instead Asymptotic function 15 frames for 1 quantity 
	// to a maximum of 300 frames for infinite quantity.
	m_scrollTime = (300.0f * quantity) / (quantity + 19.0f);

	scrollRecords(target - m_record_current);
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// scrollRecords:
//
// Init scrolling parameters to scroll left (dir=-1) or right (dir=1).
// G. Wolberg, James Hohman
//
void CoverflowWidget::scrollRecords(int dir)
{
	// do nothing if we are already scrolling
	if (m_bScrolling) return;

	// Check boundaries.
	// If target is at the last record (n), and user submits 
	// dir +1 scroll right do nothing.
	if ((m_record_target >= m_recordCount - 1) && (dir > 0))
		return;

	// If target is at the first record (0), and user submits 
	// dir -1 scroll left do nothing.
	if ((m_record_target <= 0) && (dir < 0))
		return;

	m_record_target += dir;

	// init scrolling params
	// m_scrollDir moves the records in the opposite desired
	// direction, so we negate the direction upon submit.
	// This keeps the direction of scrolling intuitive with
	// current displayed index.
	m_bScrolling = 1;		// flag scrolling condition
	m_scrollDir = -dir;		// save scrolling direction

	// register a timer callback (scrollTimer) to be triggered in a
	// specified number of milliseconds
	QTimer::singleShot(m_redraw_rate, this, SLOT(scrollTimer()));
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// scrollTimer:
//
// Timer callback function. Update scrolling parameters.
// G. Wolberg, James Hohman
//
void CoverflowWidget::scrollTimer()
{
	static unsigned int counter = 0;

	// update velocity and position
	counter++;
	m_scrollOffset = (float)m_scrollDir * counter * m_scrollUpdateInterval
		/ m_scrollTime;

	// last iteration, reset all animation parameters
	if (fabs(m_scrollOffset) >= 1 ||
		counter >= m_scrollTime / (m_scrollUpdateInterval)) {
		m_shift -= m_scrollDir;
		counter = 0;
		m_scrollDir = 0;
		m_scrollOffset = 0;
		m_bScrolling = 0;
		// set the number of frames for next animation
		m_scrollTime = 15.0f;
		update();
	}
	else {
		update();
		QTimer::singleShot(m_redraw_rate, this, SLOT(scrollTimer()));
	}
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// drawRecords:
//
// Draw all records.
// Variant function that calculates a subset of the m_records array to
// display at any given point improving performance and visual appeal.
// Dynamically calculates spacing between records based on AR: 
// m_spacing is set during resizeGL().
// Wider display means more space up to 250%, Narrower display
// means more compact up to 75%.
// G. Wolberg, James Hohman
//
void CoverflowWidget::drawRecords()
{
	// We don't draw records until the m_records struct
	// has been initialized.
	if (!m_records_loaded)
		return;

	int	 i, j, k, r, middle, incoming, outgoing;
	float	 front, center_dist, w2;
	vector3f pos;

	// center of the bounding box
	pos[0] = 0;
	pos[1] = -m_records[0].height / 2.0;
	pos[2] = -6;

	// center scene
	glTranslatef(pos[0], pos[1], pos[2]);
	front = 2.0;
	center_dist = 5.0;
	m_record_current = middle = m_recordCount / 2 + m_shift;

	if (m_scrollOffset > 0) {		// scroll right
		incoming = middle - 1;
		outgoing = middle;
	}
	else {			// scroll left
		incoming = middle + 1;
		outgoing = middle;
	}

	// If m_draw_record_max >= m_recordCount do all
	if (m_recordCount > m_draw_record_max) {
		// get range for bound
		r = m_draw_record_max / 2;

		// set lower bound for display
		j = middle - r;
		if (j < 0)
		{
			// the new range accounting for the missing
			// leftward records.
			r = r - j;
			j = 0;
		}

		// set upper bound for display
		k = middle + r + 1;
		if (k > m_recordCount)
		{
			r = r + k - m_recordCount;
			j = middle - r;
			k = m_recordCount;
		}
		// leftmost record x-position
		pos[0] = ((-(middle - j)) - center_dist + m_scrollOffset - 1) * m_spacing;
	}
	else {
		j = 0;
		k = m_recordCount;
		// leftmost record x-position
		pos[0] = (-middle - center_dist + m_scrollOffset - 1) * m_spacing;
	}

	// leftmost record position
	pos[1] = 0;
	pos[2] = 0;
	glPushMatrix();
	glTranslatef(pos[0], pos[1], pos[2]);

	// draw all shifting (non-rotating) records
	for (i = j; i<k; i++) {
		// move over records that do not purely translate
		if (i == incoming || i == outgoing) {
			glTranslatef((1 + center_dist) * m_spacing, 0, 0);
			continue;
		}

		glTranslatef(1 * m_spacing, 0, 0);
		bool flip = (i<middle);
		drawRecord(&m_records[i], flip);
	}
	glPopMatrix();

	// draw central incoming rotating record
	w2 = m_records[middle].width / 2.0;
	if (m_scrollOffset < 0) {
		pos[0] = (1 + m_scrollOffset) * -w2 + (m_scrollOffset) * (center_dist + 1) * m_spacing;
		pos[1] = 0;
		pos[2] = (1 + m_scrollOffset) * front;
		glPushMatrix();
		glTranslatef(pos[0], pos[1], pos[2]);
		glRotatef(90 - 90.0*m_scrollOffset, 0, 1, 0);
		glTranslatef(0, 0, m_records[middle].width);
	}
	else {
		pos[0] = (1 - m_scrollOffset) * w2 + (m_scrollOffset) * (center_dist + 1) * m_spacing;
		pos[1] = 0;
		pos[2] = (1 - m_scrollOffset) * front;
		glPushMatrix();
		glTranslatef(pos[0], pos[1], pos[2]);
		glRotatef(90.0 - 90.0*m_scrollOffset, 0, 1, 0);
	}

	drawRecord(&m_records[middle]);
	glPopMatrix();

	// draw central outgoing rotating record
	if (m_scrollOffset <= 0) {
		pos[0] = -w2*m_scrollOffset + (center_dist + 1)*(1 + m_scrollOffset) * m_spacing;
		pos[1] = 0;
		pos[2] = m_scrollOffset * -front;

		glTranslatef(pos[0], pos[1], pos[2]);
		glRotatef(-90.0*m_scrollOffset, 0, 1, 0);

		// In the case that we reach the end of the record struct,
		// incoming will be equal to m_recordCount, a 1-indexed number.
		// This prevents from drawing records outside the struct/index.
		if (incoming < m_recordCount)
			drawRecord(&m_records[incoming]);
	}
	else {
		pos[0] = -w2*m_scrollOffset - (center_dist + 1)*(1 - m_scrollOffset) * m_spacing;
		pos[1] = 0;
		pos[2] = m_scrollOffset * front;

		glTranslatef(pos[0], pos[1], pos[2]);
		glRotatef(-90.0*m_scrollOffset, 0, 1, 0);
		drawRecord(&m_records[outgoing - 1], true);
	}
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// drawRecord:
//
// Draw one record.
// Modified to create QOpenGLTexture from QImage.
// G. Wolberg, James Hohman
//
void CoverflowWidget::drawRecord(Record *record, bool flip)
{
	// Map the record QImage to an openGL texture.
	QOpenGLTexture *oglTexture = new QOpenGLTexture(record->qimg);
	oglTexture->setMinificationFilter(QOpenGLTexture::LinearMipMapLinear);
	oglTexture->setMagnificationFilter(QOpenGLTexture::Linear);
	// Render with texture
	oglTexture->bind();

	//// enable texture mapping and bind image to polygon
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, oglTexture->textureId());

	// draw filled polygon
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glColor3f(0.8f, 0.8f, 0.7f);
	float w = record->width;
	// draw the record normal to the viewing plane (into z)
	if (!flip) {
		glBegin(GL_QUADS);
		glTexCoord2f(1, 1);	glVertex3f(0.0f, 0.0f, 0.0f);
		glTexCoord2f(0, 1);	glVertex3f(0.0f, 0.0f, -w);
		glTexCoord2f(0, 0);	glVertex3f(0.0f, w, -w);
		glTexCoord2f(1, 0);	glVertex3f(0.0f, w, 0.0f);
		glEnd();
	}
	else {
		glBegin(GL_QUADS);
		glTexCoord2f(0, 1);	glVertex3f(0.0f, 0.0f, 0.0f);
		glTexCoord2f(1, 1);	glVertex3f(0.0f, 0.0f, -w);
		glTexCoord2f(1, 0);	glVertex3f(0.0f, w, -w);
		glTexCoord2f(0, 0);	glVertex3f(0.0f, w, 0.0f);
		glEnd();
	}
	glDisable(GL_TEXTURE_2D);

	// draw polygon outline
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glEnable(GL_LINE_SMOOTH);
	glColor3f(0.25f, 0.25f, 0.95f);
	glBegin(GL_QUADS);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.0f, -w);
	glVertex3f(0.0f, w, -w);
	glVertex3f(0.0f, w, 0.0f);
	glEnd();
	glDisable(GL_LINE_SMOOTH);

	// free the pointer
	delete oglTexture;
}
