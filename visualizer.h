// ======================================================================
// visualizer widget
// Copyright (C) 2015 by David Francisco Ayerbe
//
// visualizer.h - OpenGL widget class
//
// Written by: David Francisco Ayerbe
// ======================================================================
#ifndef VISUALIZER_H
#define VISUALIZER_H

//#include "GL/glu.h" // Do not include this.
#include <QOpenGLWidget>
#include <map>


using namespace std;

class VisualizerWidget : public QOpenGLWidget
{
	Q_OBJECT

public:
	double rando[10];
	int rand_color[10];
	explicit VisualizerWidget(QWidget *parent = 0);
	void location();


protected:
	void initializeGL();

	void paintGL();

	void resizeGL(int width, int height);

private:
	int color;

	QTimer* timer;

	

	private slots:

	public slots :

		void repaint();

	void startTime();

	void endTime();



};

#endif // VISUALIZER_H


