// ======================================================================
// tunes player
// Copyright (C) 2015 by James Hohman
//
// MainWindow.cpp - Main Window widget class
//
// Written by: James Hohman, Francisco Ayerbe, 2015
// ======================================================================

#include <QtWidgets>
#include <QMediaPlayer>
#include "MainWindow.h"
//#include <QDebug>
#include <taglib.h>
#include <fileref.h>
#include <id3v2tag.h>
#include <id3v2frame.h>
#include <id3v2header.h>
#include <attachedpictureframe.h>
#include <mpegfile.h>
#include <map>
#include "coverflow.h"
#include "visualizer.h"


using namespace std;

enum { TITLE, TRACK, TIME, ARTIST, ALBUM, GENRE, PATH };
const int COLS = PATH;


bool caseInsensitive(const QString &s1, const QString &s2)
{
	return s1.toLower() < s2.toLower();
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// MainWindow::MainWindow:
//
// Constructor. Initialize user-interface elements.
// James Hohman
//
MainWindow::MainWindow(QString program)
	: m_directory(".")
{
	// Init the player
	player = new QMediaPlayer();
	// m_progress_bar_value = 0;
	m_mmAlbumArt = new multimap<QString, QString>;
	m_mmAlbumArt_unique = new multimap<QString, QString>;

	// setup GUI
	createActions();  // create actions for each menu item
	createMenus();  // Create menus and associate actions
	createWidgets();
	createDockWindows();
	createLayouts();

	// set main window titlebar
	QString title = QString("qTunes");
	setWindowTitle(title);

	// set central widget and default size
	setCentralWidget(m_mainWidget);

	qApp->installEventFilter(this);
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// MainWindow::~MainWindow:
//
// Destructor. Save settings.
//
MainWindow::~MainWindow() {}

//// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//// Exit application confirmation
//// James Hohman
////
//void MainWindow::closeEvent(QCloseEvent *event) 
//{
//	event->ignore();
//
//	// confirm exit dialog box
//	QMessageBox m_exit_messageBox;
//	m_exit_messageBox.setStandardButtons(QMessageBox::Yes|QMessageBox::No);
//	m_exit_messageBox.setDefaultButton(QMessageBox::No);
//	m_exit_messageBox.setWindowTitle("Exit Confirmation");
//	m_exit_messageBox.setText(tr("Are you sure you wish to exit?"));
//	// Set dialog to have Question Icon
//	m_exit_messageBox.setIcon(QMessageBox::Question);
//	// Hide Window Icon like a system dialog.
//	m_exit_messageBox.setWindowFlags(Qt::Dialog | Qt::CustomizeWindowHint | Qt::WindowTitleHint);
//		
//	if (m_exit_messageBox.exec() == QMessageBox::Yes)
//	{
//		event->accept();
//	}
//}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// MainWindow::createActions:
//
// Create actions to associate with menu and toolbar selection.
// James Hohman
//
void MainWindow::createActions()
{
	// Create Open File Action
	m_actionOpen = new QAction("&Open File", this);
	// Set keyboard shortcut
	m_actionOpen->setShortcut(tr("Ctrl+O"));
	// Set associated menu Icon
	m_actionOpen->setIcon(QApplication::style()->standardIcon(QStyle::SP_FileLinkIcon));
	// Connect menu action to slot function
	connect(m_actionOpen, SIGNAL(triggered()), this, SLOT(s_open()));

	m_actionOpenFolder = new QAction("&Open Folder", this);
	m_actionOpenFolder->setShortcut(tr("Ctrl+E"));
	m_actionOpenFolder->setIcon(QApplication::style()->standardIcon(QStyle::SP_DirOpenIcon));
	connect(m_actionOpenFolder, SIGNAL(triggered()), this, SLOT(s_openFolder()));

	m_actionExit = new QAction("E&xit", this);
	m_actionExit->setShortcut(tr("Alt+F4"));
	connect(m_actionExit, SIGNAL(triggered()), this, SLOT(close()));
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// MainWindow::createMenus:
//
// Create menus and install in menubar.
// James Hohman
//
void MainWindow::createMenus()
{
	m_menuFile = menuBar()->addMenu("&File");
	m_menuFile->addAction(m_actionOpen);
	m_menuFile->addAction(m_actionOpenFolder);
	m_menuFile->addSeparator();
	m_menuFile->addAction(m_actionExit);

	m_menuWindows = menuBar()->addMenu("&Windows");
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// MainWindow::createWidgets
// James Hohman, Francisco Ayerbe
//
void MainWindow::createWidgets()
{
	// Instantiate main widget for window's central widget.
	m_mainWidget = new QWidget();
	m_mainWidget->setMinimumHeight(156);
	m_mainWidget->setMinimumWidth(200);

	// Initialize filename labels
	m_filename_label = new QLabel(QString("<strong>Song:</strong> "));
	m_filename_label->setFixedHeight(24);
	m_filename = new QLabel();
	m_filename->setFixedHeight(24);
	m_filename->setFrameStyle(QFrame::Box | QFrame::Sunken);

	// Initialize all player controls: start, stop, play, etc...
	for (int i = 0; i < ENUM_PLAYER_CTRLS_END; i++) {
		player_controls[i] = new QPushButton();
		player_controls[i]->setMinimumHeight(24);
		player_controls[i]->setDisabled(true);
	}

	// Set player control button icons
	player_controls[PREVIOUS_TRACK]->setIcon(QApplication::style()->standardIcon(QStyle::SP_MediaSkipBackward));
	player_controls[NEXT_TRACK]->setIcon(QApplication::style()->standardIcon(QStyle::SP_MediaSkipForward));
	player_controls[PLAY]->setIcon(QApplication::style()->standardIcon(QStyle::SP_MediaPlay));
	player_controls[STOP]->setIcon(QApplication::style()->standardIcon(QStyle::SP_MediaStop));

	// Connect player controls to appropriate slot functions
	QObject::connect(player_controls[PREVIOUS_TRACK], SIGNAL(clicked()), this, SLOT(s_player_previous()));
	QObject::connect(player_controls[NEXT_TRACK], SIGNAL(clicked()), this, SLOT(s_player_next()));
	QObject::connect(player_controls[PLAY], SIGNAL(clicked()), this, SLOT(s_player_play()));
	QObject::connect(player_controls[STOP], SIGNAL(clicked()), this, SLOT(s_player_stop()));

	// Initialize all player audio controls.
	for (int i = 0; i < ENUM_AUDIO_CTRLS_END; i++) {
		m_audio_controls[i] = new QPushButton();
		m_audio_controls[i]->setMinimumHeight(24);
	}
	m_audio_controls[VOLUME_MUTE]->setIcon(QApplication::style()->standardIcon(QStyle::SP_MediaVolume));

	QObject::connect(m_audio_controls[VOLUME_MUTE], SIGNAL(clicked()), this, SLOT(s_player_mute()));

	// Initialize volume slider
	m_volume_slider = new QSlider(Qt::Horizontal, this);
	m_volume_slider->setRange(0, 100);
	m_volume_slider->setMinimumHeight(24);
	m_volume_slider->setMinimumWidth(96);

	QObject::connect(m_volume_slider, SIGNAL(sliderMoved(int)), this, SLOT(changeVolume(int)));
	m_volume_slider->setValue(50);

	// Initialize song duration status to zero.
	m_player_status = new QLabel();
	m_player_status->setFixedHeight(24);
	m_player_status->setText("00:00 / 00:00");
	m_player_status->setFrameStyle(QFrame::Box | QFrame::Sunken);

	// Initialize song progress meter
	m_player_progress_bar = new QProgressBar();
	m_player_progress_bar->setFixedHeight(24);
	m_player_progress_bar->setRange(0, 100);

	// Initialize song position slider
	m_player_slider = new QSlider(Qt::Horizontal, this);
	m_player_slider->setFixedHeight(24);
	m_player_slider->setRange(0, 100);
	m_player_slider->setDisabled(true);
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// MainWindow::createLayouts:
// James Hohman
//
void MainWindow::createLayouts()
{
	// The main Layout
	QVBoxLayout *layout_main = new QVBoxLayout();

	// The player layout, stays fixed on top.
	QVBoxLayout *layout_player = new QVBoxLayout(m_mainWidget);
	layout_player->addStretch(0);
	// Player component layouts
	QHBoxLayout *layout_file = new QHBoxLayout();
	QHBoxLayout *layout_player_status = new QHBoxLayout();
	QHBoxLayout *layout_controls = new QHBoxLayout();
	QHBoxLayout *layout_progress_bar = new QHBoxLayout();

	// Build player layout
	layout_player->addLayout(layout_file);
	layout_player->addLayout(layout_player_status);
	layout_player->addLayout(layout_controls);
	layout_player->addLayout(layout_progress_bar);
	layout_main->setAlignment(Qt::AlignHCenter);

	// Build Main layout
	layout_main->addWidget(m_mainWidget);

	layout_file->addWidget(m_filename_label, 0);
	layout_file->addWidget(m_filename, 1);

	// Add player controls to the controls layout
	for (int i = 0; i < ENUM_PLAYER_CTRLS_END; i++) {
		layout_controls->addWidget(player_controls[i]);
	}
	// Add spacer for aesthetic purposes
	layout_controls->addSpacerItem(new QSpacerItem(50, 1));
	// Add audio controls to the controls layout
	for (int i = 0; i < ENUM_AUDIO_CTRLS_END; i++) {
		layout_controls->addWidget(m_audio_controls[i]);
	}
	layout_controls->addWidget(m_volume_slider);

	// Song position slider and status
	layout_player_status->addWidget(m_player_slider);
	layout_player_status->addWidget(m_player_status);

	// Song progress meter
	layout_progress_bar->addWidget(m_player_progress_bar);
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Player dock functions
// James Hohman, Francisco Ayerbe
//
void MainWindow::createDockWindows()
{
	// Playlist /////////////////////////////////////////////
	dock_playlist = new QDockWidget(tr("Playlist"), this, 0);
	dock_playlist->setAllowedAreas(Qt::LeftDockWidgetArea |
		Qt::RightDockWidgetArea |
		Qt::BottomDockWidgetArea |
		Qt::DockWidgetArea_Mask);

	// initialize table widget: complete song data
	m_playList = new QTableWidget(0, ENUM_PL_END);
	QHeaderView *headerPL = new QHeaderView(Qt::Horizontal, m_playList);
	m_playList->setHorizontalHeader(headerPL);
	m_playList->setHorizontalHeaderLabels(QStringList() <<
		"Song" << "Time" << "Path");
	m_playList->setAlternatingRowColors(1);
	m_playList->setShowGrid(1);
	m_playList->setEditTriggers(QAbstractItemView::NoEditTriggers);
	m_playList->setSelectionBehavior(QAbstractItemView::SelectRows);
	m_playList->hideColumn(PLPATH);
	m_playList->setShowGrid(false);
	dock_playlist->setWidget(m_playList);
	dock_playlist->setHidden(true);
	addDockWidget(Qt::RightDockWidgetArea, dock_playlist);
	m_menuWindows->addAction(dock_playlist->toggleViewAction());

	connect(m_playList, SIGNAL(itemDoubleClicked(QTableWidgetItem*)),
		this, SLOT(s_pl_play(QTableWidgetItem*)));

	// Visualizer /////////////////////////////////////////
	dock_visualizer = new QDockWidget(tr("Visualizer"), this, 0);
	dock_visualizer->setAllowedAreas(Qt::LeftDockWidgetArea |
		Qt::RightDockWidgetArea |
		Qt::BottomDockWidgetArea |
		Qt::DockWidgetArea_Mask);

	// initialize table widget: complete song data
	m_v_widget = new VisualizerWidget(dock_visualizer);

	dock_visualizer->setWidget(m_v_widget);
	dock_visualizer->setHidden(true);
	addDockWidget(Qt::RightDockWidgetArea, dock_visualizer);
	m_menuWindows->addAction(dock_visualizer->toggleViewAction());
	
	// Coverflow //////////////////////////////////////////////
	dock_coverflow = new QDockWidget(tr("Coverflow"), this, 0);
	dock_coverflow->setAllowedAreas(Qt::LeftDockWidgetArea |
		Qt::RightDockWidgetArea |
		Qt::BottomDockWidgetArea |
		Qt::DockWidgetArea_Mask);

	// initialize table widget: complete song data
	m_cf_widget = new CoverflowWidget(dock_coverflow);

	dock_coverflow->setWidget(m_cf_widget);
	dock_coverflow->setHidden(true);
	addDockWidget(Qt::RightDockWidgetArea, dock_coverflow);
	m_menuWindows->addAction(dock_coverflow->toggleViewAction());

	// Media Library /////////////////////////////////////////
	dock_media_library = new QDockWidget(tr("Media Library"), this, 0);
	dock_media_library->setAllowedAreas(Qt::LeftDockWidgetArea |
		Qt::RightDockWidgetArea |
		Qt::BottomDockWidgetArea |
		Qt::DockWidgetArea_Mask);

	m_ml_widget = new QWidget(dock_media_library);

	// initialize splitter
	m_ml_split = new QSplitter(Qt::Vertical, m_ml_widget);

	m_labelSide[0] = new QLabel("Filters");
	m_labelSide[0]->setAlignment(Qt::AlignCenter);
	m_labelSide[1] = new QLabel("Library");
	m_labelSide[1]->setAlignment(Qt::AlignCenter);

	// initialize label on right side of main splitter
	for (int i = 0; i<3; i++) {
		// make label widget with centered text and sunken panels
		m_label[i] = new QLabel;
		m_label[i]->setAlignment(Qt::AlignCenter);
		m_label[i]->setFrameStyle(QFrame::Panel | QFrame::Sunken);
	}

	// initialize label text
	m_label[0]->setText("<b>Genre<\b>");
	m_label[1]->setText("<b>Artist<\b>");
	m_label[2]->setText("<b>Album<\b>");

	// initialize list widgets: genre, artist, album
	for (int i = 0; i<3; i++)
		m_panel[i] = new QListWidget;

	// initialize table widget: complete song data
	m_table = new QTableWidget(0, COLS);
	headerML = new QHeaderView(Qt::Horizontal, m_table);

	m_table->setHorizontalHeaderLabels(QStringList() <<
		"Name" << "Track" << "Time" << "Artist" << "Album" << "Genre");
	headerML->setSectionsClickable(TRUE);
	m_table->setHorizontalHeader(headerML);
	m_table->setAlternatingRowColors(1);
	m_table->setShowGrid(1);
	m_table->setEditTriggers(QAbstractItemView::NoEditTriggers);
	m_table->setSelectionBehavior(QAbstractItemView::SelectRows);

	connect(headerML, SIGNAL(sectionClicked(int)), this, SLOT(sortCol(int)));
	
	// init signal/slot connections
	connect(m_panel[0], SIGNAL(itemClicked(QListWidgetItem*)),
		this, SLOT(s_ml_genre_filter_clicked(QListWidgetItem*)));
	connect(m_panel[1], SIGNAL(itemClicked(QListWidgetItem*)),
		this, SLOT(s_ml_artist_filter_clicked(QListWidgetItem*)));
	connect(m_panel[2], SIGNAL(itemClicked(QListWidgetItem*)),
		this, SLOT(s_ml_album_filter_clicked(QListWidgetItem*)));
	connect(m_table, SIGNAL(itemDoubleClicked(QTableWidgetItem*)),
		this, SLOT(s_ml_add_play(QTableWidgetItem*)));

	// Media Library Dock Layout
	// create a 2-row, 3-column grid layout, where row0 and
	// row1 consist of labels and list widgets, respectively.
	QWidget	    *ml_filter_widget = new QWidget(this);
	QHBoxLayout *hbox_filter = new QHBoxLayout(ml_filter_widget);
	QWidget     *ml_inner_filter_widget = new QWidget(this);
	QGridLayout *grid = new QGridLayout(ml_inner_filter_widget);
	grid->addWidget(m_label[0], 0, 0);
	grid->addWidget(m_label[1], 0, 1);
	grid->addWidget(m_label[2], 0, 2);
	grid->addWidget(m_panel[0], 1, 0);
	grid->addWidget(m_panel[1], 1, 1);
	grid->addWidget(m_panel[2], 1, 2);

	hbox_filter->addWidget(m_labelSide[0]);
	hbox_filter->addWidget(ml_inner_filter_widget);

	// table layout
	QWidget	    *ml_table_widget = new QWidget(this);
	QHBoxLayout *hbox = new QHBoxLayout(ml_table_widget);
	hbox->addWidget(m_labelSide[1]);
	hbox->addWidget(m_table);

	// add widgets to the splitters
	m_ml_split->addWidget(ml_filter_widget);
	m_ml_split->addWidget(ml_table_widget);

	dock_media_library->setWidget(m_ml_widget);
	addDockWidget(Qt::BottomDockWidgetArea, dock_media_library);
	m_menuWindows->addAction(dock_media_library->toggleViewAction());
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// MainWindow::setSizes:
//
// Set splitter sizes.
// G. Wolberg
//
void MainWindow::setSizes(QSplitter *splitter, int size1, int size2)
{
	QList<int> sizes;
	sizes.append(size1);
	sizes.append(size2);
	splitter->setSizes(sizes);
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// get_song_meta
//
// Reads taglib and sets song meta into an array.
// If there are problems with the tag, returns false. Otherwise, true.
// James Hohman
//
bool MainWindow::get_song_meta(TagLib::FileRef *f, QString *m_song_meta)
{
	// Init m_song_meta (not required, but the way we will handle
	// conditions makes this necessary)	
	m_song_meta[TITLE] = "";
	m_song_meta[TRACK] = "";
	m_song_meta[DURATION] = "";
	m_song_meta[ARTIST] = "";
	m_song_meta[ALBUM] = "";
	m_song_meta[GENRE] = "";

	//qDebug() << "Filename: " << s;
	if (f->isNull()) {
		// Null file.
		// qDebug() << "Null file.";
		return false;
	}
	else if (!f->tag()) {
		// Not a tag.
		// qDebug() << "Not a tag.";
		return false;
	}
	else if (f->tag()->isEmpty()) {
		// No tags. Still try to retrieve song duration.
		// qDebug() << "No tags.";
		if (f->audioProperties()) {
			m_song_meta[DURATION] = sec_to_timeString(
				f->audioProperties()->length() - 1);
		}
		return false;
	}
	else {
		// If we have valid tag, extract metadata
		m_song_meta[TITLE] = f->tag()->title().to8Bit(true).data();
		m_song_meta[TRACK] = QString::number(f->tag()->track());
		m_song_meta[ARTIST] = f->tag()->artist().to8Bit(true).data();
		m_song_meta[ALBUM] = f->tag()->album().to8Bit(true).data();
		m_song_meta[GENRE] = f->tag()->genre().to8Bit(true).data();
	}

	// If we have audio properties extract duration.
	if (f->audioProperties()) {
		m_song_meta[DURATION] = sec_to_timeString(
			f->audioProperties()->length() - 1);
		return true;
	}
	else {
		// No audio properties.
		// qDebug() << "No audioProperties.";
		return false;
	}
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// get_id3_album_art
//
// Reads album art from id3 tag via taglib from provided filename and
// returns it as a resized 
// (m_album_art_max_dim x m_album_art_max_dim) QImage.
// If no such album art exists, returns a QImage of default 
// album art: :/albumArt/default_album.jpg.
// James Hohman
//
QImage MainWindow::get_id3_album_art(QString filename) {
	QImage img;
	// https://rajeevandlinux.wordpress.com/2012/04/24/extract-album-art-from-mp3-files-using-taglib-in-c/
	// http://stackoverflow.com/questions/4752020/how-do-i-use-taglib-to-read-write-coverart-in-different-audio-formats 
	// http://stackoverflow.com/questions/26733505/how-to-get-the-cover-art-of-a-mp3-file-with-taglib-c
	static const char *IdPicture = "APIC";
	// Initialize an TagLib::MPEG::File object from provided filename.
	TagLib::MPEG::File mpegFile(QFile::encodeName(filename).constData());
	//TagLib::MPEG::File mpegFile(QFile::encodeName("C:/Music/Song.mp3").constData());
	// Initialize id3v2tag pointer from the file
	TagLib::ID3v2::Tag *id3v2tag = mpegFile.ID3v2Tag();
	TagLib::ID3v2::FrameList Frame;
	TagLib::ID3v2::AttachedPictureFrame *PicFrame;

	// if an id3v2tag exists, attempt to extract the image.
	if (id3v2tag)
	{
		Frame = id3v2tag->frameList(IdPicture);
		if (!Frame.isEmpty())
		{
			for (TagLib::ID3v2::FrameList::ConstIterator it = Frame.begin();
				it != Frame.end(); ++it)
			{
				PicFrame = (TagLib::ID3v2::AttachedPictureFrame *)(*it);
				img.loadFromData((const uchar*)PicFrame->picture().data(),
					PicFrame->picture().size());
				// Return a scaled version of the image
				// for performance considerations.
				return img.scaled(m_album_art_max_dim,
					m_album_art_max_dim, Qt::KeepAspectRatio);
			}
		}
	}

	// Doesn't have id3v2tag or the Frame was empty.
	// Return default album art image.
	// URL Scheme doesn't work
	// char name[] = "qrc:///albumArt/default_album.jpg";
	char name[] = ":/albumArt/default_album.jpg";

	if (!img.load(name))
	{
		// qDebug() << "Error loading " << name;
	}
	return img;
}


// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// MainWindow::traverseDirs:
// Prof. Wolberg's Template
//
// Traverse all subdirectories and collect filenames into m_listSongs.
// G. Wolberg, James Hohman, Francisco Ayerbe
//
void MainWindow::traverseDirs(QString path)
{
	QString		key, val;
	QStringList	list;

	// init listDirs with subdirectories of path
	QDir dir(path);
	dir.setFilter(QDir::AllDirs | QDir::NoDotAndDotDot);
	QFileInfoList listDirs = dir.entryInfoList();

	// init listFiles with all *.mp3 files in path
	QDir files(path);
	files.setFilter(QDir::Files);
	files.setNameFilters(QStringList("*.mp3"));
	QFileInfoList listFiles = files.entryInfoList();

	for (int i = 0; i < listFiles.size(); i++) {
		// init list with default values: ""
		for (int j = 0; j <= COLS; j++)
			list.insert(j, "");

		// store file pathname into 0th position in list
		QFileInfo fileInfo = listFiles.at(i);
		list.replace(PATH, fileInfo.filePath());

		// Update ProgressBar
		if (i == 0) {
			m_progressBar_current_total = m_progressBar_current_total + listFiles.size();
		}
		m_progressBar_cumulative_total++;
		m_progressBar_value = (int)(m_progressBar_cumulative_total * 100 / m_progressBar_current_total);
		m_progressBar->setLabelText(
			fileInfo.fileName() + "\n" + QString::number(m_progressBar_cumulative_total) +
			"/" + QString::number(m_progressBar_current_total));
		m_progressBar->setValue(m_progressBar_value);
		QApplication::processEvents();

		// convert it from QString to Ascii and store in source
		// Note: this uses audiere. Replace with function from Qt5 multimedia module
		// http://stackoverflow.com/questions/10579419/taglib-could-not-open-file
		source = new TagLib::FileRef(QFile::encodeName(fileInfo.filePath()).constData());

		if (get_song_meta(source, m_song_meta)) {
			for (int x = 0; x < ENUM_SONG_META_END; x++) {
				enum_song_meta meta = (enum_song_meta)x;
				list.replace(meta, m_song_meta[meta]);
			}
		}
		else {
			continue;
		}

		// Free the pointer for future iterations.
		delete source;

		// append list (song data) into songlist m_listSongs;
		// uninitialized fields are empty strings
		m_listSongs << list;
	}

	// base case: no more subdirectories
	if (listDirs.size() == 0) return;

	// recursively descend through all subdirectories
	for (int i = 0; i<listDirs.size(); i++) {
		QFileInfo fileInfo = listDirs.at(i);
		traverseDirs(fileInfo.filePath());
	}

	return;
}


// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// MainWindow::initLists:
// Prof. Wolberg's Template
//
// Populate lists with data (first time).
// G. Wolberg, James Hohman, Francisco Ayerbe
//
void MainWindow::initLists()
{
	// error checking
	if (m_listSongs.isEmpty()) return;

	// Can't declare this in the header for some reason?
	// This was because we weren't using the std namespace in the header. Fixed.
	// multimap<QString, QString> *m_mmAlbumArt;
	delete m_mmAlbumArt;
	m_mmAlbumArt = new multimap<QString, QString>;

	//multimap<QString, QString> *m_mmAlbumArt_unique;
	delete m_mmAlbumArt_unique;
	m_mmAlbumArt_unique = new multimap<QString, QString>;

	// create separate lists for genres, artists, and albums
	for (int i = 0; i<m_listSongs.size(); i++) {
		m_listGenre << m_listSongs[i][GENRE];
		m_listArtist << m_listSongs[i][ARTIST];
		m_listAlbum << m_listSongs[i][ALBUM];
		m_mmAlbumArt->insert(pair<QString, QString>(m_listSongs[i][ALBUM], m_listSongs[i][PATH]));
	}

	// sort each list
	qStableSort(m_listGenre.begin(), m_listGenre.end(), caseInsensitive);
	qStableSort(m_listArtist.begin(), m_listArtist.end(), caseInsensitive);
	qStableSort(m_listAlbum.begin(), m_listAlbum.end(), caseInsensitive);

	// add each list to list widgets, filtering out repeated strings
	for (int i = 0; i<m_listGenre.size(); i += m_listGenre.count(m_listGenre[i]))
		m_panel[0]->addItem(m_listGenre[i]);

	for (int i = 0; i<m_listArtist.size(); i += m_listArtist.count(m_listArtist[i]))
		m_panel[1]->addItem(m_listArtist[i]);

	// We create a multimap object that holds [0] album name, [1] album path.
	// But this object has non-unique albums, so we need to create a multimap that contains
	// only unique album names (so we don't display for example, 10 of each album).
	// From the unique album list (m_mmAlbumArt_unique) we can give this
	// structure to the coverflow widget.
	multimap<QString, QString>::iterator it = m_mmAlbumArt->begin();
	int k = 0;
	for (int i = 0; i < m_listAlbum.size(); i += m_listAlbum.count(m_listAlbum[i]))
	{
		m_panel[2]->addItem(m_listAlbum[i]);

		// build the unique album art list.
		m_mmAlbumArt_unique->insert(pair<QString, QString>((*it).first, (*it).second));
		k = m_listAlbum.count(m_listAlbum[i]);
		std::advance(it, k);
	}

	// copy data to table widget
	QTableWidgetItem *item[COLS];
	for (int i = 0; i<m_listSongs.size(); i++) {
		m_table->insertRow(i);
		for (int j = 0; j<COLS; j++) {
			item[j] = new QTableWidgetItem;
			item[j]->setText(m_listSongs[i][j]);
			item[j]->setTextAlignment(Qt::AlignLeft);
			m_table->setItem(i, j, item[j]);
		}
	}
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// MainWindow::init_MediaLibrary:
//
// Reinitializes the media library, clear and reset.
// James Hohman
//
void MainWindow::init_MediaLibrary()
{
	// Refresh the lists for subsequent runs
	m_panel[0]->clear();
	m_panel[1]->clear();
	m_panel[2]->clear();
	// clearContents preserves the table headers, must manually set table rowCount back to zero.
	m_table->clearContents();
	m_table->setRowCount(0);
	m_listSongs.clear();
	m_listGenre.clear();
	m_listAlbum.clear();
	m_listArtist.clear();
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Slot functions
// MainWindow::s_open:
//
// Slot function for File|Open
// James Hohman
//
void MainWindow::s_open()
{
	// open a file dialog box
	QFileDialog *fd = new QFileDialog;

	fd->setFileMode(QFileDialog::ExistingFile);
	QString s = fd->getOpenFileName(this, tr("Select Music File"), m_directory
		, tr("Music Files (*.mp3)"));

	// check if cancel was selected.
	if (s == NULL) return;

	this->play_song(s);
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// MainWindow::s_openFolder:
//
// Slot function for File|Open Folder
// James Hohman
//
void MainWindow::s_openFolder()
{
	// open a file dialog box
	QFileDialog *fd = new QFileDialog;

	fd->setFileMode(QFileDialog::DirectoryOnly);
	QString s = fd->getExistingDirectory(this, tr("Select Music Directory"), m_directory,
		QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);

	// check if cancel was selected.
	if (s == NULL) return;

	// Re-initialize the media library
	init_MediaLibrary();

	// copy full pathname of selected file into m_directory.
	m_directory = s;

	// init progress bar
	m_progressBar_value = 0;
	m_progressBar_cumulative_total = 0;
	m_progressBar_current_total = 0;
	m_progressBar = new QProgressDialog(this);
	m_progressBar->setWindowTitle("Updating");
	m_progressBar->setFixedSize(300, 100);
	m_progressBar->setCancelButtonText("Cancel");
	m_progressBar->setRange(0, 100);
	m_progressBar->setValue(0);
	m_progressBar->setModal(true);
	m_progressBar->setAutoReset(false);
	m_progressBar->setAutoClose(false);
	m_progressBar->setMinimumDuration(2000);

	// Recursively traverse directories
	traverseDirs(m_directory);
	m_progressBar->setLabelText("All Done!");
	m_progressBar->close();
	// Initialize media library lists
	initLists();
	// If the coverflow dock is visible, initialize
	// the coverflow widget with unique record multimap
	if (!dock_coverflow->isHidden()) {
		m_cf_widget->init(m_mmAlbumArt_unique);
	}
	// Free the pointer.
	delete m_progressBar;
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Play song function
// Without autoplay argument, then by default don't autoplay.
// James Hohman, Francisco Ayerbe
void MainWindow::play_song(QString s) {
	// copy full pathname of selected file into m_directory.
	// m_directory = s;
	m_filename->setText(s);
	m_filename->setToolTip(s);

	// http://stackoverflow.com/questions/10579419/taglib-could-not-open-file
	TagLib::FileRef *f = new TagLib::FileRef(QFile::encodeName(s).constData());

	// Build the song label for the player
	if (get_song_meta(f, m_song_meta)) {
		m_filename->setText(
			m_song_meta[ARTIST] + ": " +
			m_song_meta[ALBUM] + " - " +
			m_song_meta[TITLE]);
	}

	// Set the player duration label
	if (f->audioProperties()) {
		// Duration is +1 second as compared to player duration.
		playerStatusInit(f->audioProperties()->length() - 1);
	}
	else {
		// Initialize the player status label.
		// BUT, player can't get duration until playback starts. Weird.
		// Have to manually get metadata.
		qint64 dur = player->duration();
		if (dur >= 0) {
			playerStatusInit(dur / 1000);
		}
		else {
			playerStatusInit(dur);
		}
	}

	// free the file pointer
	delete f;

	// set QMediaPlayer Widget to the song
	player->setMedia(QUrl::fromLocalFile(s));

	// Connect signals/slots for update position sliders,
	// progress bar, player controls
	connect(player, SIGNAL(durationChanged(qint64)),
		this, SLOT(s_player_duration_ready(qint64)));
	connect(player, SIGNAL(positionChanged(qint64)),
		this, SLOT(playerStatusUpdate(qint64)));
	connect(player, SIGNAL(positionChanged(qint64)),
		this, SLOT(s_player_slider_update(qint64)));
	connect(player, SIGNAL(stateChanged(QMediaPlayer::State)),
		this, SLOT(s_player_state(QMediaPlayer::State)));
	connect(m_player_slider, SIGNAL(sliderMoved(int)),
		this, SLOT(s_player_slider_position_change(int)));
	// Initialize all the controls to be usable
	for (int i = 0; i < ENUM_PLAYER_CTRLS_END; i++) {
		enum_player_ctrls ctrl = (enum_player_ctrls)i;
		player_controls[i]->setDisabled(false);

	}
	// If playlist doesn't exist then we can't
	// next or prev track. Disable these buttons.
	if (pl_exist == FALSE){
		player_controls[NEXT_TRACK]->setDisabled(TRUE);
		player_controls[PREVIOUS_TRACK]->setDisabled(TRUE);
	}
	// Enable the song position slider.
	m_player_slider->setDisabled(false);
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Play song function
// With autoplay argument.
// James Hohman, Francisco Ayerbe
void MainWindow::play_song(QString s, bool autoplay) {
	// copy full pathname of selected file into m_directory.
	// m_directory = s;
	m_filename->setText(s);
	m_filename->setToolTip(s);

	// http://stackoverflow.com/questions/10579419/taglib-could-not-open-file
	TagLib::FileRef *f = new TagLib::FileRef(QFile::encodeName(s).constData());

	// Build the song label for the player
	if (get_song_meta(f, m_song_meta)) {
		m_filename->setText(
			m_song_meta[ARTIST] + ": " +
			m_song_meta[ALBUM] + " - " +
			m_song_meta[TITLE]);
	}

	// Set the player duration label
	if (f->audioProperties()) {
		// Duration is +1 second as compared to player duration.
		playerStatusInit(f->audioProperties()->length() - 1);
	}
	else {
		// Initialize the player status label.
		// BUT, player can't get duration until playback starts. Weird.
		// Have to manually get metadata.
		qint64 dur = player->duration();
		if (dur >= 0) {
			playerStatusInit(dur / 1000);
		}
		else {
			playerStatusInit(dur);
		}
	}

	// free the file pointer
	delete f;

	// set QMediaPlayer Widget to the song
	player->setMedia(QUrl::fromLocalFile(s));

	// Connect signals/slots for update position sliders,
	// progress bar, player controls
	connect(player, SIGNAL(durationChanged(qint64)),
		this, SLOT(s_player_duration_ready(qint64)));
	connect(player, SIGNAL(positionChanged(qint64)),
		this, SLOT(playerStatusUpdate(qint64)));
	connect(player, SIGNAL(positionChanged(qint64)),
		this, SLOT(s_player_slider_update(qint64)));
	connect(player, SIGNAL(stateChanged(QMediaPlayer::State)),
		this, SLOT(s_player_state(QMediaPlayer::State)));
	connect(m_player_slider, SIGNAL(sliderMoved(int)),
		this, SLOT(s_player_slider_position_change(int)));
	// Initialize all the controls to be usable
	for (int i = 0; i < ENUM_PLAYER_CTRLS_END; i++) {
		enum_player_ctrls ctrl = (enum_player_ctrls)i;
		player_controls[i]->setDisabled(false);
	}
	// If playlist doesn't exist then we can't
	// next or prev track. Disable these buttons.
	if (pl_exist == FALSE){
		player_controls[NEXT_TRACK]->setDisabled(TRUE);
		player_controls[PREVIOUS_TRACK]->setDisabled(TRUE);
	}

	// Enable the song position slider.
	m_player_slider->setDisabled(false);

	// Play the song if autoplay == true
	if (autoplay) { player->play(); }
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// MainWindow::playerStatusInit
// Initialize the status label based on player media metadata
// James Hohman
//
void MainWindow::playerStatusInit(qint64 media_duration)
{
	// init m_track_duration
	if (media_duration >= 0) {
		m_track_duration = (int)media_duration;
	}
	// set the player duration label
	m_player_status->setText(sec_to_timeString(0) + " / " + sec_to_timeString(media_duration));
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Helper member function to convert duration in ms to 
// string time format mm:ss or hh:mm:ss.
// if time is unknown returns --:--.
// James Hohman
QString MainWindow::sec_to_timeString(qint64 time_in_sec) {
	QString format = "mm:ss";
	if (time_in_sec > 3600)
		format = "hh:mm:ss";
	if (time_in_sec >= 0) {
		QTime totalTime((time_in_sec / 3600) % 60, (time_in_sec / 60) % 60,
			time_in_sec % 60, (time_in_sec * 1000) % 1000);
		return totalTime.toString(format);
	}
	else {
		return "--:--";
	}
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Helper member function to update track duration display and
// song position slider
// When duration is ready we'll receive it here
// James Hohman
void MainWindow::s_player_duration_ready(qint64 media_duration)
{
	// make media_duration available.
	m_track_duration = (int)(media_duration / 1000);
	m_player_slider->setRange(0, m_track_duration);
}


// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Member function to update progress bar and song position slider
// James Hohman
void MainWindow::playerStatusUpdate(qint64 media_position)
{
	// Update timestring label
	qint64 currentInfo = media_position / 1000;
	m_player_status->setText(sec_to_timeString(currentInfo) + " / " + sec_to_timeString(m_track_duration));

	// Update progress bar
	m_progress_bar_value = (int)((media_position * 100) / player->duration());
	m_player_progress_bar->setValue(m_progress_bar_value);

	// Update player slider
	m_player_slider->setValue((int)currentInfo);
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Player slot functions
//

// Francisco Ayerbe
void MainWindow::sortCol(int a)
{
	m_table->sortByColumn(a, Qt::AscendingOrder);
}

// Francisco Ayerbe
void MainWindow::s_player_previous() {

	if (current_playlist_location == 0){
		current_playlist_location = m_playList->rowCount() - 1;
	}

	else current_playlist_location--;

	if (player->state() == QMediaPlayer::PlayingState){
		this->play_song(m_playList->item(current_playlist_location, PLPATH)->text(), true);
	}

	if (player->state() == QMediaPlayer::StoppedState){
		this->play_song(m_playList->item(current_playlist_location, PLPATH)->text(), true);
		player->stop();
	}

	if (player->state() == QMediaPlayer::PausedState){
		this->play_song(m_playList->item(current_playlist_location, PLPATH)->text(), true);
		player->pause();
	}


}

// Francisco Ayerbe
void MainWindow::s_player_next() {
	if (current_playlist_location == m_playList->rowCount() - 1){
		current_playlist_location = 0;
	}
	else current_playlist_location++;

	if (player->state() == QMediaPlayer::PlayingState){
		this->play_song(m_playList->item(current_playlist_location, PLPATH)->text(), true);
	}

	if (player->state() == QMediaPlayer::StoppedState){
		this->play_song(m_playList->item(current_playlist_location, PLPATH)->text(), true);
		player->stop();
	}

	if (player->state() == QMediaPlayer::PausedState){
		this->play_song(m_playList->item(current_playlist_location, PLPATH)->text(), true);
		player->pause();
	}
}


// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Slot function to play QMediaPlayer
// James Hohman
void MainWindow::s_player_play()
{
	player->play();
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Slot function to pause QMediaPlayer
// James Hohman
void MainWindow::s_player_pause()
{
	player->pause();
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Slot function to stop QMediaPlayer
// James Hohman
void MainWindow::s_player_stop()
{
	player->stop();
}


// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Slot function to change Volume
// Francisco Ayerbe
void MainWindow::changeVolume(int volume)
{
	player->setVolume(volume);
}


// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Slot function to mute QMediaPlayer
// Francisco Ayerbe
void MainWindow::s_player_mute()
{
	if (player->isMuted() == FALSE)
	{
		player->setMuted(TRUE);

	}

	else player->setMuted(FALSE);

	m_audio_controls[VOLUME_MUTE]->setIcon(style()->standardIcon(player->isMuted()
		? QStyle::SP_MediaVolumeMuted
		: QStyle::SP_MediaVolume));
}


// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Slot function to update QSlider position
// James Hohman
void MainWindow::s_player_slider_update(qint64 media_position)
{
	// update slider position from media position
	m_player_slider->setValue((int)(media_position / 1000));
}


// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Slot function for QSlider position change: Update QMediaPlayer
// song position and update slider position
// James Hohman
void MainWindow::s_player_slider_position_change(int slider_position)
{
	qint64 slider_pos_64 = (qint64)slider_position;
	slider_pos_64 = slider_pos_64 * 1000;

	// Bug: https://bugreports.qt.io/browse/QTBUG-32746
	// When player is paused, and player position is updated
	// player resumes playback.
	player->setPosition(slider_pos_64);
}


// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Slot function for QMediaPlayer state change: play, pause, stop
// Update player controls to the proper icon/function.
// Perform actions on player state change.
// James Hohman
void MainWindow::s_player_state(QMediaPlayer::State state)
{
	// Player stopped, update play button to play
	if (state == QMediaPlayer::State::StoppedState) {
		playerStatusUpdate(0);

		// Switch to play icon/function.
		player_controls[PLAY]->setIcon(QApplication::style()->standardIcon(QStyle::SP_MediaPlay));
		QObject::connect(player_controls[PLAY], SIGNAL(clicked()), this, SLOT(s_player_play()));
		m_v_widget->endTime();
	}
	// player playing, update play button to pause
	if (state == QMediaPlayer::State::PlayingState) {
		// Switch to pause icon/function.
		player_controls[PLAY]->setIcon(QApplication::style()->standardIcon(QStyle::SP_MediaPause));
		QObject::connect(player_controls[PLAY], SIGNAL(clicked()), this, SLOT(s_player_pause()));
		m_v_widget->startTime();
	}
	// player paused, update play button to play
	if (state == QMediaPlayer::State::PausedState) {
		// Switch to play icon/function.
		player_controls[PLAY]->setIcon(QApplication::style()->standardIcon(QStyle::SP_MediaPlay));
		QObject::connect(player_controls[PLAY], SIGNAL(clicked()), this, SLOT(s_player_play()));
		m_v_widget->endTime();
	}
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Slot function for Media Library, double-click
// Media Library Double Click: Add to Playlist, then Play
// James Hohman, Francisco Ayerbe
void MainWindow::s_ml_add_play(QTableWidgetItem *ml_item) {
	// Add the Media library item into the Playlist 
	int plCount = m_playList->rowCount();
	int ml_row = ml_item->row();
	current_playlist_location = plCount;
	m_playList->insertRow(plCount);
	//current_playlist_size++;
	QTableWidgetItem *plitem[ENUM_PL_END];
	for (int j = 0; j<ENUM_PL_END; j++) {
		plitem[j] = new QTableWidgetItem;
		plitem[j]->setTextAlignment(Qt::AlignLeft);
	}
	plitem[PLSONG]->setText(
		m_listSongs[ml_row][ARTIST] + ": " +
		m_listSongs[ml_row][ALBUM] + " - " +
		m_listSongs[ml_row][TITLE]);
	m_playList->setItem(plCount, PLSONG, plitem[PLSONG]);
	plitem[PLDURATION]->setText(m_listSongs[ml_row][DURATION]);
	m_playList->setItem(plCount, PLDURATION, plitem[PLDURATION]);
	plitem[PLPATH]->setText(m_listSongs[ml_row][PATH]);
	m_playList->setItem(plCount, PLPATH, plitem[PLPATH]);

	// And play it
	this->play_song(m_listSongs[ml_row][PATH], true);
	pl_exist = TRUE;

}



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Slot function for Playlist, double-click
// Playlist Double Click: Play Item
// When you double click a song in the playlist it
// should play.
// James Hohman, Francisco Ayerbe
//
void MainWindow::s_pl_play(QTableWidgetItem *pl_item) {
	int pl_row = pl_item->row();
	this->play_song(m_playList->item(pl_row, PLPATH)->text(), true);
	current_playlist_location = pl_row;
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Slot function for Media Library Album Filter, Single-Click
// When clicking the album QListWidget, we should update the
// Coverflow widget to show that item. This slot function
// accomplishes that.
// James Hohman
//
void MainWindow::s_ml_album_filter_clicked(QListWidgetItem* item) {
	if (dock_coverflow->isVisible()) {
		m_cf_widget->animateToIndex(item->listWidget()->row(item));
	};
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// eventFilter to capture keystrokes
// James Hohman
// Could not get this to work from within the coverflow class
// so I brought it here.
//
bool MainWindow::eventFilter(QObject* object, QEvent* event)
{
	if (event->type() == QEvent::KeyPress)
	{
		QKeyEvent *keyEvent = dynamic_cast<QKeyEvent *>(event);
		//MainWindow *theparent = dynamic_cast<MainWindow *>(object);
		// Should only send requests if coverflow is actually visible.
		if (dock_coverflow->isVisible()) {
			// key left scroll left 1, update selected Album in Media Library
			if (keyEvent->key() == Qt::Key_Left)
			{
				m_cf_widget->scrollRecords(-1);
				m_panel[2]->setCurrentRow(m_cf_widget->m_record_target);
			}
			// key right scroll right 1, update selected Album in Media Library
			else if (keyEvent->key() == Qt::Key_Right)
			{
				m_cf_widget->scrollRecords(1);
				m_panel[2]->setCurrentRow(m_cf_widget->m_record_target);
			}
			// key up increase zoom by 1
			else if (keyEvent->key() == Qt::Key_Up)
			{
				m_cf_widget->zoomRecords(-1.0f);
				qApp->processEvents();
			}
			// key up decrease zoom by 1
			else if (keyEvent->key() == Qt::Key_Down)
			{
				m_cf_widget->zoomRecords(1.0f);
				qApp->processEvents();
			}
		}

		return true;
	}
	else
	{
		// standard event processing
		return QObject::eventFilter(object, event);
	}
};