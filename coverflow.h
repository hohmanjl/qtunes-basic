// ======================================================================
// coverflow widget
// Copyright (C) 2015 by James Hohman
//
// coverflow.h - OpenGL widget class
//
// Written by: James Hohman, 2015
// Based on Prof. Wolberg's Example.
// ======================================================================
#ifndef COVERFLOW_H
#define COVERFLOW_H

//#include "GL/glu.h" // Do not include this.
#include <QOpenGLWidget>
#include <map>


using namespace std;

class CoverflowWidget : public QOpenGLWidget
{
	Q_OBJECT

public:
	// Constructor
	explicit CoverflowWidget(QWidget *parent = 0);

	// User-defined datatypes
	typedef float vector3f[3];
	// Typedef struct {
	struct Record {
		int width;
		int height;
		char imageFilename[512];
		QImage qimg;
	};

	// Function prototypes
	void init(multimap<QString, QString> *mmAlbum);
	void scrollRecords(int dir);
	void zoomRecords(float dir);
	void drawRecords();
	void drawRecord(Record *record, bool flip = false);
	void animateToIndex(int target);

	// global variables
	Record *m_records;  // list of records (albums)
	int m_recordCount;  // number of records
	int m_shift;
	float m_scrollOffset;  // -1<offset<1 current scroll position
	int m_scrollDir;  // current scroll velocity
	float m_scrollTime;  // total scrolling time (per push)
	int m_scrollUpdateInterval;  // scrolling time increment
	int m_bScrolling;  // scrolling boolean: 1=true; 0=false
	int m_redraw_rate;  // redraw rate in ms
	// maximum number of records drawn in coverflow at any given time.
	static const int m_draw_record_max = 11;
	int m_record_target; // the record target, with which we should animate towards and focus
	int m_record_current; // the current record
	float m_zoom; // zoom increment
	float m_spacing; // spacing scaler between records.
	bool m_records_loaded; // set when record data has been initialized.

protected:
	void initializeGL();
	void paintGL();
	void resizeGL(int width, int height);

public slots:
	void scrollTimer();
};

#endif