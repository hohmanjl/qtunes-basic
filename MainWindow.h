// ======================================================================
// tunes player
// Copyright (C) 2015 by James Hohman
//
// MainWindow.h - Main Window widget class
//
// Written by: James Hohman, Francisco Ayerbe, 2015
// ======================================================================



#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtWidgets>
#include <QMediaPlayer>
#include <taglib.h>
#include <fileref.h>
#include <coverflow.h>
#include <visualizer.h>
#include <map>

using namespace std;

// MainWindow Class

class MainWindow : public QMainWindow {
	Q_OBJECT

public:
	//! Constructor.
	MainWindow(QString);

	//! Destructor.
	~MainWindow();

	// Public functions
	QString sec_to_timeString(qint64 time_in_sec);
	QImage get_id3_album_art(QString filename);

	// multimap of unique album art for coverflow
	multimap<QString, QString> *m_mmAlbumArt_unique;
	CoverflowWidget *m_cf_widget;

	VisualizerWidget *m_v_widget;


	public slots:
	// slots
	// Must be public or won't work.
	// menu actions
	void s_open();
	void s_openFolder();
	//void s_toggle_playlist();

	// player controls
	void s_player_play();
	void s_player_pause();
	void s_player_stop();
	void s_player_mute();
	void s_player_previous();
	void s_player_next();
	void changeVolume(int);
	void playerStatusInit(qint64 media_duration);
	void playerStatusUpdate(qint64 media_position);
	void s_player_state(QMediaPlayer::State state);
	void s_player_slider_update(qint64 media_position);
	void s_player_slider_position_change(int slider_position);
	void s_player_duration_ready(qint64 media_duration);
	void s_ml_add_play(QTableWidgetItem *);
	void s_pl_play(QTableWidgetItem *);
	void play_song(QString);
	void play_song(QString, bool);
	void sortCol(int a);
	void s_ml_album_filter_clicked(QListWidgetItem*);

private:
	// player controls
	enum enum_player_ctrls {
		PREVIOUS_TRACK,
		PLAY, STOP,
		NEXT_TRACK,
		ENUM_PLAYER_CTRLS_END
	};

	// player audio controls
	enum enum_audio_ctrls {
		VOLUME_MUTE,
		ENUM_AUDIO_CTRLS_END
	};

	// song meta
	enum enum_song_meta {
		TITLE, TRACK, DURATION, ARTIST, ALBUM, GENRE,

		ENUM_SONG_META_END
	};

	// song meta
	enum enum_playlist_meta {
		PLSONG, PLDURATION, PLPATH, ENUM_PL_END
	};

	void createActions();
	void createMenus();
	void createWidgets();
	void createLayouts();
	void createDockWindows();
	//void closeEvent(QCloseEvent *event);
	bool get_song_meta(TagLib::FileRef *f, QString *m_song_meta);
	bool pl_exist = FALSE;


	void setSizes(QSplitter *, int, int);
	void initLists();
	void traverseDirs(QString);

	void init_MediaLibrary();

	int current_playlist_location = 0;
	//int current_playlist_size = 0;
	int m_progress_bar_value;
	// Track would have to be 50 days long
	// before this overflows int type.
	int m_track_duration;

	int m_progressBar_value;
	qint64 m_progressBar_cumulative_total;
	qint64 m_progressBar_current_total;

	// Media
	QMediaPlayer *player;
	//QMediaObject *player_current_media;

	// acions
	QAction *m_actionOpen;
	QAction *m_actionOpenFolder;
	QAction *m_actionExit;

	// menus
	QMenu *m_menuFile;
	QMenu *m_menuWindows;

	// strings
	QString m_directory;

	// widgets
	QWidget *m_mainWidget;
	QLabel *m_filename_label;
	QLabel *m_filename;
	QLabel *m_player_status;
	QSlider *m_player_slider;
	QSlider *m_volume_slider;
	QPushButton *player_controls[ENUM_PLAYER_CTRLS_END];
	QPushButton *m_audio_controls[ENUM_AUDIO_CTRLS_END];
	QProgressBar *m_player_progress_bar;
	QTableWidget *m_playList;
	QDockWidget *dock_playlist;

	// Media Library
	QDockWidget *dock_media_library;
	QWidget *m_ml_widget;
	QSplitter *m_ml_split;
	QLabel *m_labelSide[2];
	QLabel *m_label[3];



	// Coverflow
	QDockWidget *dock_coverflow;
	QDockWidget *dock_visualizer;

	QProgressDialog	*m_progressBar;
	QListWidget 	*m_panel[3];
	QTableWidget	*m_table;
	QHeaderView *headerML;

	// string lists
	QString    m_song_meta[ENUM_SONG_META_END];
	QStringList	   m_listGenre;
	QStringList	   m_listArtist;
	QStringList	   m_listAlbum;
	QList<QStringList> m_listSongs;


	TagLib::FileRef *source; // TagLib file reference pointer

	multimap<QString, QString> *m_mmAlbumArt; // album art map (album, path)

	static const int m_album_art_max_dim = 256; // maximum album art dimensions for performance/memory

protected:
	bool eventFilter(QObject* object, QEvent* event);
};

#endif // MAINWINDOW_H