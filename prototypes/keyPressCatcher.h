// keyPressCatcher.h file
// http://www.codeprogress.com/cpp/libraries/qt/showQtExample.php?key=QtInstallEventFilterExample&index=172
// Modified: James Hohman
// 4/27/2015

#ifndef KEYPRESSCATCHER_H
#define KEYPRESSCATCHER_H

#include <QtGui>

class keyPressCatcher : public QObject
{
public:
	keyPressCatcher() : QObject()
	{};
	~keyPressCatcher()
	{};

	bool eventFilter(QObject* object, QEvent* event)
	{
		if (event->type() == QEvent::KeyPress)
		{
			QKeyEvent *keyEvent = dynamic_cast<QKeyEvent *>(event);
			MainWindow *theparent = dynamic_cast<MainWindow *>(object);
			if (keyEvent->key() == Qt::Key_Left)
				theparent->m_cf_widget->scrollRecords(-1);
			else if (keyEvent->key() == Qt::Key_Right)
				theparent->m_cf_widget->scrollRecords(1);
			else if (keyEvent->key() == Qt::Key_Up)
				theparent->m_cf_widget->zoomRecords(-0.1);
			else if (keyEvent->key() == Qt::Key_Down)
				theparent->m_cf_widget->zoomRecords(0.1);

			return true;
		}
		else
		{
			// standard event processing
			return QObject::eventFilter(object, event);
		}
	};
};

#endif