// ======================================================================
// alt_coverflow widget
// Copyright (C) 2015 by James Hohman
//
// alt_coverflow.cpp - OpenGL widget class
//
// Written by: James Hohman, 2015
// Based on Prof. Wolberg's Example.
// ======================================================================
// See comments in alt_coverflow.h
#include "coverflow.h"
#include "GL/glu.h"
#include <QDebug>
#include <QOpenGLTexture>
#include <map>
#include <QtWidgets>
#include <taglib.h>
#include <fileref.h>
#include <id3v2tag.h>
#include <id3v2frame.h>
#include <id3v2header.h>
#include <attachedpictureframe.h>
#include <mpegfile.h>
//#include "MainWindow.h"


//using namespace std;

CoverflowWidget::CoverflowWidget(QWidget *parent)
: QOpenGLWidget(parent)
{
	m_recordCount = 0;  // total number of records
	m_draw_record_max = 5;  // maximum number of records drawn in coverflow at any given time.
	m_record_target = 0; // the record target, with which we should animate towards and focus
	m_record_current = 0; // the current record
	m_shift = 0;
	m_scrollOffset = 0.0;  // -1<offset<1 current scroll position
	m_scrollTime = 150.0;  // total scrolling time (per push)
	m_scrollUpdateInterval = 5;  // scrolling time increment
	m_bScrolling = 0;  // scrolling boolean: 1=true; 0=false
	m_force_redraw = false;  // flag to force paintGL update
	m_zoom = -1.0; // zoom increment
	zinc = 1.0;
	animation_offset = 0.0;

	update_interval = 250; // 17 ms = 60 fps
	max_framerate = 1000 / 60; // 17 ms = 60 fps

	animation_interval = 500; // 17 ms = 60 fps

	time_diff = 0;
	oldtime = clock();

	animator = new QTimer();
	connect(animator, SIGNAL(timeout()), this, SLOT(slot_drawRecords()));
	initializeGL();
}

void CoverflowWidget::initializeGL() {
	glEnable(GL_DEPTH_TEST);
	glClearColor(0, 0, 0, 0);
}

void CoverflowWidget::resizeGL(int width, int height) {
	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(90., (float)width / height, 1.0, 1000);
	glMatrixMode(GL_MODELVIEW);
}

void CoverflowWidget::paintGL() {
	display();
	//if (m_recordCount > 0) {
	//	newtime = clock();
	//	time_diff = unsigned long(newtime - oldtime);
	//	qDebug() << time_diff;
	//	if ((time_diff > update_interval) || (m_force_redraw)) {
	//		m_force_redraw = false;
	//		display();
	//		oldtime = clock();
	//	}
	//}
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// init:
//
// Initialize data structure of records when given an album map.
// The input is recordCount, which is the number of record albums.
// This number should always be odd for display symmetry.
//
void CoverflowWidget::init(std::multimap<QString, QString> *mmAlbum)
{
	// init global vars and create space
	m_recordCount = mmAlbum->size();
	//m_records = (Record*)malloc(sizeof(Record)* m_recordCount);
    m_records = new Record[m_recordCount];
	// init data structure with dimensions of cover art polygons
	// and filenames for the polygon textures
	int i = 0;
	for (std::multimap<QString, QString>::iterator it = mmAlbum->begin(); it != mmAlbum->end(); ++it)
	{
		// save dimensions
		m_records[i].width = 5;
		m_records[i].height = 5;

		// save image filenames
		strcpy(m_records[i].imageFilename, (*it).second.toLocal8Bit().data());

		// From MainWindow public method, get the id3 album art.
		//m_records[i].qimg = ((MainWindow*)(parent()))->get_id3_album_art(m_records[i].imageFilename);
		i++;
	}
	// Make sure we redraw as soon as possible.
	//m_force_redraw = true;
	// Cannot call paintGL or display here leads to corrupted window??
	//QApplication::processEvents();
	//paintGL();
	//initializeGL();
	//this->paintGL();
	//resizeGL(640, 320);
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// display:
//
// Display handler routine.
//
void CoverflowWidget::display()
{
	// clear color and depth buffers
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
	//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();

	// draw all records
	drawRecords();

	// update the screen
	//glutSwapBuffers();
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// zoomRecords:
//
// Zoom in or out up (dir=-0.1) or down (dir=0.1).
//
void CoverflowWidget::zoomRecords(float dir)
{
	// Set zoom.
	m_zoom += dir;

	qDebug() << "Zoom: " << m_zoom;
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// scrollRecords:
//
// Init scrolling parameters to scroll left (dir=-1) or right (dir=1).
//
void CoverflowWidget::scrollRecords(int dir)
{
	// do nothing if we are already scrolling
	//if (m_bScrolling) return;

	// init scrolling params
	old_animation_time = clock();
	m_bScrolling = 1;		// flag scrolling condition
	m_scrollDir = dir;		// save scrolling direction

	// Set the target record for display within bounds.
	m_record_target += dir;
	if (m_record_target <= 0) {
		m_record_target = 0;
	}
	else if (m_record_target >= m_recordCount) {
		m_record_target = m_recordCount;
	}

	qDebug() << "Target: " << m_record_target;

	// register a timer callback (scrollTimer) to be triggered in a
	// specified number of milliseconds
	//glutTimerFunc(m_scrollUpdateInterval, scrollTimer, 0);
	qDebug() << "animator: " << animator;
	if ((!animator->isActive()) || (m_record_target != m_record_current)) {
		//animator->start(max_framerate);
		animator->start(100);
	}
}


// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// drawRecords:
//
// Draw all records.
//
void CoverflowWidget::drawRecords()
{
	if (m_recordCount < 1)
		return;

	qDebug() << "drawRecords --------------------";
	int	 i, middle, incoming, outgoing;
	float	 front, center_dist, w2;
	vector3f pos;

	// center of the bounding box
	pos[0] = 0;
	pos[1] = -0.3; // (-m_records[0].height / 2.0) + m_zoom;
	pos[2] = -0.5;

	// center scene
	glTranslatef(pos[0], pos[1], pos[2]);
	qDebug() << "1. Center Position (x, y, z): " << pos[0] << pos[1] << pos[2];
	front = 0.0;
	center_dist = 0.0;
	middle = m_draw_record_max / 2 + m_shift;
	if (m_scrollOffset > 0) {		// scroll right
		incoming = middle - 1;
		outgoing = middle;
	}
	else {			// scroll left
		incoming = middle + 1;
		outgoing = middle;
	}

	if (m_record_target != m_record_current) {
		// Do the animation, at the end of the animation (animation_interval)
		// current = target and target is facing the user.
		m_force_redraw = true;
		new_animation_time = clock();
		unsigned long timedelta = unsigned long(new_animation_time - old_animation_time);
		if (timedelta >= animation_interval)
			timedelta = animation_interval;
		float animation_position = (float)timedelta / (float)animation_interval;
		if (m_record_target > m_record_current) {
			animation_offset += 0.001;
		}
		else if (m_record_target < m_record_current) {
			animation_offset -= 0.001;
		}
		qDebug() << "td: " << timedelta << "animation_position: " << animation_position;
	}
	else {
		if (animator->isActive()) {
			animator->stop();
			qDebug() << "timer stopped.";
		}
	}

	// leftmost record position
	pos[0] = -middle - center_dist + m_scrollOffset - 1 + animation_offset;
	glPushMatrix();
	glTranslatef(pos[0], pos[1], pos[2]);
	qDebug() << "2. Position Left (x, y, z): " << pos[0] << pos[1] << pos[2];

	if (m_draw_record_max > 1) {
		// draw all shifting (non-rotating) records
		for (i = 0; i < m_draw_record_max; i++) {
			if (i == 0) {
				// Left of Middle Rotate 90 about y-axis
				glRotatef(90.0, 0, 1, 0);
			}

			glTranslatef(0, 0, zinc);
			qDebug() << "4. Non-Middle Position (x, y, z): " << 0 << 0 << zinc ;

			bool flip = false;

			drawRecord(&m_records[i], flip);

			//if (i == 0) {
			//	// Left of Middle Rotate 90 about y-axis
			//	glRotatef(90.0, 0, 1, 0);
			//}
			//else if (i == middle + 1) {
			//	// Right of Middle Rotate 180 (flip) about y-axis
			//	glRotatef(-180.0, 0, 1, 0);
			//	zinc = -zinc;
			//}

			//if (i == middle) {
			//	glTranslatef(1, 0, 0);
			//	qDebug() << "3. Middle Position (x, y, z): " << 1 << 0 << 0;
			//}
			//else {
			//	glTranslatef(0, 0, zinc + animation_offset);
			//	qDebug() << "4. Non-Middle Position (x, y, z): " << 0 << 0 << zinc + animation_offset;
			//}

			//// move over records that do not purely translate
			//if (i == incoming || i == outgoing) {
			//	//glTranslatef(1 + center_dist, 0, 0);
			//	continue;
			//}

			//bool flip = (i < middle);

			//drawRecord(&m_records[i], flip);
		}
		glPopMatrix();
	}

	//// draw central incoming rotating record
	//w2 = m_records[middle].width / 2.0;
	//if (m_scrollOffset <= 0) {
	//	pos[0] = (1 + m_scrollOffset) *  -w2 +
	//		(m_scrollOffset)* (center_dist + 1);
	//	pos[1] = 0;
	//	pos[2] = (1 + m_scrollOffset) * front;
	//}
	//else {
	//	pos[0] = (1 - m_scrollOffset) *   w2 +
	//		(m_scrollOffset)* (center_dist + 1);
	//	pos[1] = 0;
	//	pos[2] = (1 - m_scrollOffset) * front;
	//}
	//glPushMatrix();
	//glTranslatef(pos[0], pos[1], pos[2]);
	//qDebug() << "5. Position (x, y, z): " << pos[0] << pos[1] << pos[2];

	//// rotate it between 90 and 180 degrees if scrollOffset < 0
	//// rotate it between 90 and  0  degrees if scrollOffset > 0
	//if (m_scrollOffset < 0) {
	//	glRotatef(-90.0*m_scrollOffset, 0, 1, 0);
	//}
	//else if (m_scrollOffset > 0) {
	//	glRotatef(-90.0*m_scrollOffset, 0, 1, 0);
	//}

	//// translate so that it rotates around the other edge
	//if (m_scrollOffset <= 0) {
	//	glTranslatef(0, 0, m_records[middle].width);
	//	qDebug() << "6. Position (x, y, z): " << 0 << 0 << m_records[middle].width;
	//}

	//drawRecord(&m_records[middle]);
	//glPopMatrix();

	//// draw central outgoing rotating record
	//if (m_scrollOffset <= 0) {
	//	pos[0] = -w2*m_scrollOffset + (center_dist + 1)*(1 + m_scrollOffset) - 1.5;
	//	pos[1] = 0;
	//	pos[2] = m_scrollOffset * -front;

	//	glTranslatef(pos[0], pos[1], pos[2]);
	//	qDebug() << "7. Position (x, y, z): " << pos[0] << pos[1] << pos[2];
	//	glRotatef(-90.0*m_scrollOffset, 0, 1, 0);
	//	drawRecord(&m_records[incoming]);
	//}
	//else {
	//	pos[0] = -w2*m_scrollOffset - (center_dist + 1)*(1 - m_scrollOffset);
	//	pos[1] = 0;
	//	pos[2] = m_scrollOffset * front;

	//	glTranslatef(pos[0], pos[1], pos[2]);
	//	qDebug() << "8. Position (x, y, z): " << pos[0] << pos[1] << pos[2];
	//	glRotatef(-90.0*m_scrollOffset, 0, 1, 0);
	//	drawRecord(&m_records[outgoing - 1], true);
	//}
}



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// drawRecord:
//
// Draw one record.
//
void CoverflowWidget::drawRecord(Record *record, bool flip)
{
	//qDebug() << record->imageFilename;
	//// Map the record QImage to an openGL texture.
	//QOpenGLTexture *oglTexture = new QOpenGLTexture(record->qimg);
	//oglTexture->setMinificationFilter(QOpenGLTexture::LinearMipMapLinear);
	//oglTexture->setMagnificationFilter(QOpenGLTexture::Linear);
	//// Render with texture
	//oglTexture->bind();

	//// enable texture mapping and bind image to polygon
	//glEnable(GL_TEXTURE_2D);
	////glBindTexture(GL_TEXTURE_2D, record->texId);
	//glBindTexture(GL_TEXTURE_2D, oglTexture->textureId());

	// draw filled polygon
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glColor3f(.8, .8, .7);
	if (!flip) {
		glBegin(GL_QUADS);
		glTexCoord2f(1, 1);	glVertex3f(1.0f, 0.0f, 0.0f);
		glTexCoord2f(0, 1);	glVertex3f(0.0f, 0.0f, 0.0f);
		glTexCoord2f(0, 0);	glVertex3f(0.0f, 1.0f, 0.0f);
		glTexCoord2f(1, 0);	glVertex3f(1.0f, 1.0f, 0.0f);
		glEnd();
	}
	else {
		glBegin(GL_QUADS);
		glTexCoord2f(0, 1);	glVertex3f(0.0f, 0.0f, 0.0f);
		glTexCoord2f(1, 1);	glVertex3f(1.0f, 0.0f, 0.0f);
		glTexCoord2f(1, 0);	glVertex3f(1.0f, 1.0f, 0.0f);
		glTexCoord2f(0, 0);	glVertex3f(0.0f, 1.0f, 0.0f);
		glEnd();
	}
	//glDisable(GL_TEXTURE_2D);

	// draw polygon outline
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glEnable(GL_LINE_SMOOTH);
	glColor3f(.8, 0, 0);
	glBegin(GL_QUADS);
	glVertex3f(1.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(1.0f, 0.0f, 0.0f);
	glEnd();
	glDisable(GL_LINE_SMOOTH);
	//delete oglTexture;
}
