// ======================================================================
// alt_coverflow widget
// Copyright (C) 2015 by James Hohman
//
// alt_coverflow.h - OpenGL widget class
//
// Written by: James Hohman, 2015
// Based on Prof. Wolberg's Example.
// ======================================================================
// Alternate to coverflow. Don't have time to complete, but the idea 
// was to have a dynamic animating scene with the cursor (c)
// animating towards a target (j). The velocity would be the 
// distance j - c / dt. Instead of animating over a fixed quantity 
// of frames as in coverflow, you would animate over a fixed duration via QTimer.
// Theoretically, this would prevent "bogging" down, since slower 
// frames would be compensated by the increased draw distance.
// This code is not really usable as is... you should compare it to the 
// coverflow implementation in coverflow.cpp, coverflow.h to get an idea and 
// incorporate the timer functions from here.
#ifndef COVERFLOW_H
#define COVERFLOW_H

#include <QOpenGLWidget>
#include <iostream>
#include <fstream>
#include <cmath>
#include <cassert>
#include <cstring>
#include <ctime>
#include <map>

//using namespace std;

class CoverflowWidget : public QOpenGLWidget
{
	Q_OBJECT
public:
	explicit CoverflowWidget(QWidget *parent = 0);
	void init(std::multimap<QString, QString> *mmAlbum);
	void scrollRecords(int dir);
	void zoomRecords(float dir);

private:
	// user-defined datatypes
	typedef float vector3f[3];
	//typedef struct {
	struct Record {
		int width;
		int height;
		GLuint texId;
		char imageFilename[512];
		QImage qimg;
	};

	// function prototypes
	//void initRecords();
	void display();
	//void scrollTimer(int value);
	void drawRecords();
	void drawRecord(Record *record, bool flip = false);

	// global variables
	Record *m_records;	 // list of records (albums)
	int	m_recordCount;  // total number of records
	int m_draw_record_max;  // maximum number of records drawn in coverflow at any given time.
	int m_record_target; // the record target, with which we should animate towards and focus
	int m_record_current; // the current record
	int	m_shift;
	float m_scrollOffset;  // -1<offset<1 current scroll position
	int m_scrollDir;  // current scroll velocity
	float m_scrollTime;  // total scrolling time (per push)
	int m_scrollUpdateInterval;  // scrolling time increment
	int m_bScrolling;  // scrolling boolean: 1=true; 0=false
	bool m_force_redraw;  // flag to force paintGL update
	float m_zoom; // zoom increment
	float zinc;
	float animation_offset;

	// Variables controlling maximum update rate.
	clock_t oldtime;
	clock_t newtime;
	unsigned long time_diff;
	unsigned long update_interval; // 17 ms = 60 fps
	int max_framerate; // 17 ms = 60 fps

	// Variable controlling the animation duration in ms
	clock_t old_animation_time;
	clock_t new_animation_time;
	unsigned long animation_interval; // 17 ms = 60 fps
	QTimer *animator;

protected:
	void initializeGL();
	void paintGL();
	void resizeGL(int width, int height);

public slots :
	void slot_drawRecords() { drawRecords(); };
};

#endif